package es.cipfpbatoi.json;

import java.util.List;


public class Biblioteca {
    
    private String nombre;
    private String ubicacion;    
    private List<Libro> listaLibros;    

    public Biblioteca() {
    }    

    public List<Libro> getListaLibros() {
        return listaLibros;
    }
    public void setListaLibros(List<Libro> bl) {
        this.listaLibros = bl;
    }    

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
