package es.cipfpbatoi.xml;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

//Definición del elemento raíz de nuestro documento
@XmlRootElement(name = "library", namespace = "lib")
@XmlType(propOrder = {"nombre", "ubicacion", "listaLibros"})
public class Biblioteca {    
    private String nombre;
    private String ubicacion;    
    private List<Libro> listaLibros;    

    public Biblioteca() {
    }
    
    //@XmlElementWrapper(name = "books")
    @XmlElement(name = "book")
    public List<Libro> getListaLibros() {
        return listaLibros;
    }
    public void setListaLibros(List<Libro> bl) {
        this.listaLibros = bl;
    }    

    @XmlElement(name = "name")
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlElement(name = "location")
    public String getUbicacion() {
        return ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
